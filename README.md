Cadence Labs Test Project
=======

Summary
-----------

Fork from this repository and make a pull request which contains the following:

* A Magento module which creates a widget that can be added to a CMS page or Static Block in the admin

Widget Specifications
-----------

* The widget should have 2 parameters *start date* and *end date*, 2 text fields
* The widget should should have a corresponding block and template file (in the location of your choosing)
* The template file should output:
* The top 5 best selling SKUs in the given time period, along with their total sales (use row_total from the database)
* The sales data should be generated using a Magento resource model (however, don't cheat and use the 'reporting' models).